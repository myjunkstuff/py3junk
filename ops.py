name = "john"
if name in ["john", "rick"]:
    print("your name is on the list.")

x=[1,2,3]
y=[3,4,5]
print(x == y)
print(x is y)

print(not False)
print((not False) == (False))

primes=[2,3,5,7,8]
for prime in primes:
    print(prime)
