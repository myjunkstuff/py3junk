mylist = []
mylist.append(1)
mylist.append(2)
mylist.append(3)
print(mylist[0]) #some comment
print(mylist[1])
print(mylist[2])

for x in mylist:
    print(x)

print("\n\n")

names = ["john", "eric", "jessica"]
print(names)

name="Chuck"
age=43

print("hello, %s!" % name)
if age > 18:
    old="true"
else:
    old="false"

if old=="true":
    print("\nYou are old!\n")
